package stepdefinitions;


import cucumber.api.java.before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.then;
import cucumber.api.java.en.when;
import sun.security.util.PendingException;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCasts;
import tasks.Login;
import tasks.OpenUp;
import tasks.Search;

public class ChoucairAcademyStepDefinions {
    public void setStage(){
        Onstage.setTheStage(new OnlineCast());
    }

    @Given("^than brandon wants to learn automation at the academy Choucair$")
    public void thanBrandonwantsToLearnAutomationAtTheAcademyChoucair() {
        Onstage.theActorCalLed(requiredActor,"Brandon").wasAbleTo(OpenUp.thepage(), (Login.onThepage()));
    }

    @when("^he search for the course (.*) on the choucair academy platform$")
    public void heSearchForTheCourseRecursosAutomatizacionBancolombiaOnTheChoucairAcademyPlatform(String course) {
        OnStage.theActorIntheSpotlight().attemptsTo(Search.the(course));
    }
    @Then("^he finds the course called resources Recursos Automatizacion Bancolombia$")
    public void heFindsTheCourseCalledResourcesRecursosAutomatizacionBancolombia() throws Exception{
        throw new PendingException();
    }
}
