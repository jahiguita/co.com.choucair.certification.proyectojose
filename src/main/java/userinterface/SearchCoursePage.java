package userinterface;

import net.serenitybdd.core.pages.pageObject;
import net.serenitybdd.screenplay.targets.target;
import net.thucydides.core.annotations.Defaulturl;

import java.lang.annotation.Target;


public class SearchCoursePage extends pageObject {
    public static final Target BUTTON_UC = Target.the(targetElementName, "Selecciona la Universidad Choucair")
            .located(By.xpath("//div@='universidad']//strong"));
    public static final Target INPUT_COURSE = Target.the(targetElementName,"Buscar el curso")
            .located(By.id("coursesearchbox"));
    public static final Target BUTTON_GO = Target.the(targetElementName,"Da click para buscar el curso")
            .located(By.id("//button[@class='btn btn-secondary']"));
    public static final Target SELECT_COURSE = Target.the(targetElementName,"Da click para buscar el curso")
            .located(By.xpath("//h4[contains(text(),'Recursos Automatizacion Bancolombia')]"));

}
