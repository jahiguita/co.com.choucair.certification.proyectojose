package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Perfomable;
import net.serenitybdd.screenplay.task;
import userinterface.ChoucairAcademypage;

public class OpenUp implements task{
    private userinterface.ChoucairAcademypage ChoucairAcademypage;
    public static Performable thepage(){
        return tasks.instrumented(OpenUp.class);
    }
    @Override
    public <T extends Actor> void performas(T actor){
        actor.attemptsTo(Open.browserOn(choucairAcademypage));
    }
}
