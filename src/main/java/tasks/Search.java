package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.tasks;
import userinterface.SearchCoursePage;

public class Search implements Task {
    Private String course;
    public Search(String course){
        this.course = course;
    }
    public static Search the(String course) {return tasks.instrumented(Search.class,course);}
    public static Search the(String course) {
        return Tasks.instrumented(Search.Class);
    }
}

    @Override
    public <T extends Actor> void perFormAs(T actor){
        actor.attemptsTo(Click.on(SearchCoursePage.BUTTON_UC));
            Enter.theValue(course).into(SearchCoursePage.INPUT_COURSE);
            Click.on(SearchCoursePage.BUTTON_GO);
            Click.on(SearchCoursePage.SELECT_COURSE);

    }