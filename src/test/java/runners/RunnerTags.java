package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWhithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/academyChoucair.feature",
        tags = "@Regresion",
        glue = "co.com.choucair.certification.proyectojose.stepdefinitions",
        snippets = SnippetType.CAMELCASE
)

public class RunnerTags {
}
