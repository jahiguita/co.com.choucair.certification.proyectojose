package tasks;

import co.com.choucair.certification.academy.userinterface.ChoucairLoginPage;
import com.sun.tools.javac.comp.Enter;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import userinterface.ChoucairLoginPage;

public class Login implements task {
    public static Object onThepage() {
        return Tasks.instrumented(Login.class);
    }
    @Override
    public <T extends Actor> void performAs(T actor){
        actor.attemptsTo(Click.on(ChoucairLoginPage.LOGIN_BUTTON));
        Enter.theValue("IngresaTuUsuario").inton(choucairloginpage.INPUT_USER);
        Enter.theValue("IngresaTuContraseña*").inton(choucairloginpage.INPUT_PASSWORD);
        CLick.on(choucairloginpage.ENTER_BUTTON);

    }
}
